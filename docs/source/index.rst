.. _archimate-ru:

=================================================
 Русскоязычная документация по ArchiMate
=================================================

:author: Dmitry M. <safrmwrk@gmail.com>
:version: |version|


.. note:: Документ в процессе создания

.. toctree::
   :maxdepth: 1

   preface
   definitions
   language-structure
   motivation-elements
   strategy-elements
   examples
   references