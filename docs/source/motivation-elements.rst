.. _motivation-elements:

=======================================
Мотивационный аспект
=======================================

Элемент мотивации в нотации Archimate представляет собой абстрактный архитектурный элемент, который используется для описания факторов, мотивов, целей и ограничений, влияющих на архитектуру предприятия. Эти элементы позволяют моделировать и анализировать, что мотивирует организацию к изменениям в своей архитектуре, какие стратегические цели она ставит перед собой, какие потребности стейкхолдеров необходимо удовлетворить, и какие ограничения и требования важны для архитектурных решений. Элементы мотивации играют ключевую роль в создании связи между стратегическими целями и реализацией архитектуры, что помогает организации принимать обоснованные решения и выстраивать согласованную архитектурную стратегию.

.. csv-table:: 
   :widths: 20, 50, 30
   :delim: ;
   :header: "Элемент", "Описание", "Нотация"

   Stakeholder / Заинтересованные лица; Роль человека, команды или организации (или их подклассов), заинтересованная в эффектах от архитектуры.; |stakeholder| |stakeholder-pictogram|
   Driver / Драйвер; Состояние внутренней или внешней среды, побуждающее организацию определять свои цели и осуществлять изменения, необходимые для их достижения.; |driver| |driver-pictogram|
   Assessment / Оценка; Результат анализа состояния дел на предприятии применительно к какому-либо драйверу.; |assessment| |assessment-pictogram|
   Goal / Цель; Высокоуровневое утверждение о намерениях, направлении развития или желаемом конечном состоянии организации и ее заинтересованных сторон.; |goal| |goal-pictogram|
   Outcome / Итог; Конечный результат. Может быть как положительный, так и негативный; |outcome| |outcome-pictogram|
   Principle / Принцип; Утверждение о желаемом, определяющее общее свойство, применимое к любой системе в определенном контексте ее архитектуры.; |principle| |principle-pictogram|
   Requirement / Требование; Утверждение о необходимом, определяющее свойство, применимое к конкретной системе, что отражается в ее архитектуре.; |requirement| |requirement-pictogram|
   Constraint / Ограничение; Фактор, ограничивающий достижение целей.; |constraint| |constraint-pictogram|
   Meaning / Ценность; Относительная значимость, полезность или важность понятия.; |meaning| |meaning-pictogram|
   Value / Значение; Смысл, заключенный в понятии, или интерпретация понятия в конкретном контексте.; |value| |value-pictogram|
  

.. note:: Смотрите также :ref:`motivation-example`.


.. |stakeholder| image:: ../_static/motivation-elements/fig-Stakeholder-Notation_1.png
	:height: 6ex
.. |stakeholder-pictogram| image:: ../_static/motivation-elements/fig-Stakeholder-Notation_2.png
	:height: 2ex

.. |driver| image:: ../_static/motivation-elements/fig-Driver-Notation_1.png
	:height: 6ex
.. |driver-pictogram| image:: ../_static/motivation-elements/fig-Driver-Notation_2.png
	:height: 2ex


.. |assessment| image:: ../_static/motivation-elements/fig-Assessment-Notation_1.png
	:height: 6ex
.. |assessment-pictogram| image:: ../_static/motivation-elements/fig-Assessment-Notation_2.png
	:height: 2ex


.. |goal| image:: ../_static/motivation-elements/fig-Goal-Notation_1.png
	:height: 6ex
.. |goal-pictogram| image:: ../_static/motivation-elements/fig-Goal-Notation_2.png
	:height: 2ex


.. |outcome| image:: ../_static/motivation-elements/fig-Outcome-Notation_1.png
	:height: 6ex
.. |outcome-pictogram| image:: ../_static/motivation-elements/fig-Outcome-Notation_2.png
	:height: 2ex


.. |principle| image:: ../_static/motivation-elements/fig-Principle-Notation_1.png
	:height: 6ex
.. |principle-pictogram| image:: ../_static/motivation-elements/fig-Principle-Notation_2.png
	:height: 2ex


.. |requirement| image:: ../_static/motivation-elements/fig-Requirement-Notation_1.png
	:height: 6ex
.. |requirement-pictogram| image:: ../_static/motivation-elements/fig-Requirement-Notation_2.png
	:height: 2ex


.. |constraint| image:: ../_static/motivation-elements/fig-Constraint-Notation_1.png
	:height: 6ex
.. |constraint-pictogram| image:: ../_static/motivation-elements/fig-Constraint-Notation_2.png
	:height: 2ex


.. |meaning| image:: ../_static/motivation-elements/fig-Meaning-Notation_1.png
	:height: 6ex
.. |meaning-pictogram| image:: ../_static/motivation-elements/fig-Meaning-Notation_2.png
	:height: 2ex


.. |value| image:: ../_static/motivation-elements/fig-Value-Notation_1.png
	:height: 6ex
.. |value-pictogram| image:: ../_static/motivation-elements/fig-Value-Notation_2.png
	:height: 2ex