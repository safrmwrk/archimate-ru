.. _examples:

=======================================
Примеры архитектурных диаграмм
=======================================

.. toctree::
   :maxdepth: 1

   examples/motivation-example



Другие примеры
---------------------------------------

* `Архимейт по-русски: пример архитектурной диаграммы для реализации метода "ISO 15926 Outside" <https://ailev.livejournal.com/956191.html>`_
* `Visual Paradigm: Примеры ArchiMate <https://blog.visual-paradigm.com/ru/archimate-examples/>`_
* `Использование ArchiMate в разработке нового программного продукта <https://hostco.ru/news/ispolzovanie-archimate-v-razrabotke-novogo-programmnogo-produkta/>`_
* Стандартные и нестандартные примеры моделей в нотации Archimate (`Презентация <https://www.businessstudio.ru/upload/iblock/27f/Presentation_Koptelov.pdf>`_, `видео <https://www.youtube.com/watch?v=LMPg3bTjNu0>`_ )