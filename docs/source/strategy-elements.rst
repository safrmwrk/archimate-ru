.. _strategy-elements:


=======================================
Стратегический уровень
=======================================

Стратегический слой ArchiMate охватывает высокоуровневые аспекты архитектуры предприятия. Здесь моделируются цели и стратегии организации, а также создаваемая ценность для заинтересованных сторон. Этот слой позволяет организации определить, какие изменения необходимы для достижения своих стратегических целей и какие ресурсы и возможности доступны.

.. csv-table:: 
   :widths: 20, 50, 30
   :delim: ;
   :header: "Элемент", "Описание", "Нотация"

   Resource / Ресурс; 	Актив, принадлежащий человеку или организации или контролируемый ими; |resource| |resource-pictogram|
   Capability / Способность; Умение, которым обладает активный структурный элемент (например, организация, человек или система).; |capability| |capability-pictogram|
   Value Stream / Поток создания ценности; Последовательность действий, которые создают конечный результат для потребителя, стейкхолдера заинтересованной стороны или конечного пользователя.; |value-stream| |value-stream-pictogram|
   Course of Action/ Направление действий; Подход или план конфигурирования способностей и ресурсов предприятия, используемый для достижения цели.; |course-of-action| |course-of-action-pictogram|



.. |resource| image:: ../_static/strategy-elements/fig-Resource-Notation_1.png
	:height: 6ex

.. |resource-pictogram| image:: ../_static/strategy-elements/fig-Resource-Notation_2.png
	:height: 2ex

.. |capability| image:: ../_static/strategy-elements/fig-Capability-Notation_1.png
	:height: 6ex

.. |capability-pictogram| image:: ../_static/strategy-elements/fig-Capability-Notation_2.png
	:height: 2ex

.. |value-stream| image:: ../_static/strategy-elements/fig-Value-Stream-Notation_1.png
	:height: 6ex

.. |value-stream-pictogram| image:: ../_static/strategy-elements/fig-Value-Stream-Notation_2.png
	:height: 2ex

.. |course-of-action| image:: ../_static/strategy-elements/fig-Course-of-Action-Notation_1.png
	:height: 6ex

.. |course-of-action-pictogram| image:: ../_static/strategy-elements/fig-Course-of-Action-Notation_2.png
	:height: 2ex