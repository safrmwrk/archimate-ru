.. _motivation-example:

=======================================
Пример мотивационный диаграммы
=======================================

Описание :ref:`элементов Мотивационного аспекта <motivation-elements>`.

.. figure:: ../_static/examples/motivation-diagram-example-001.jpg
       :scale: 50 %
       :align: center
       :alt: Пример мотивационный диаграммы

       Пример мотивационный диаграммы


.. note:: Диаграмма взята из презентации `Коптелов А.К. Инструментарий Archi 4.0 между Visio и ARIS <https://koptelov.info/wp-content/uploads/2017/09/ArchiMateArchi.pdf>`_



