.. _references:

=======================================
Источники
=======================================

#. `ArchiMate® 3.2 Specification <https://pubs.opengroup.org/architecture/archimate3-doc/index.html>`_
#. `Systems Еducation: Чем полезен ArchiMate аналитику <https://systems.education/archimate>`_
#. `Курс лекций по Archimate <https://www.youtube.com/watch?v=v2xwfpit9VU&list=PLkAK0ZVQdawlmCXLXc_zBkIGErAoKOSgp&index=2>`_
#. `Архимейт по-русски <https://ailev.livejournal.com/988360.html>`_
#. BABOK. Руководство к Своду знаний по бизнес-анализу. Версия 3.0
#. `Архитектура ИТ решений. Часть 1. Архитектура предприятия <https://habr.com/ru/articles/347204/>`_
#. `Моделирование архитектуры предприятия. Обзор языка ArchiMate <https://www.cfin.ru/itm/standards/ArchiMate.shtml>`_
#. `Коптелов А.К. -- Методология ArchiMate 3.0 <https://koptelov.info/archimate_3_0/>`_
#. `Язык Archimate <https://www.businessstudio.ru/help/docs/current/doku.php/ru/manual/archimate>`_
#. Стандартные и нестандартные примеры моделей в нотации Archimate (`Презентация <https://www.businessstudio.ru/upload/iblock/27f/Presentation_Koptelov.pdf>`_, `видео <https://www.youtube.com/watch?v=LMPg3bTjNu0>`_ )

